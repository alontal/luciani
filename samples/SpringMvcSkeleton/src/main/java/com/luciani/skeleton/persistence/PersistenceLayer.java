package com.luciani.skeleton.persistence;

import com.luciani.skeleton.model.Country;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.Collection;


@Repository
public class PersistenceLayer {
    private static final Logger logger = LoggerFactory.getLogger(PersistenceLayer.class);

    ArrayList<Country> countries = new ArrayList<Country>();

    public PersistenceLayer() {
        // Add values to the mock database
        countries.add(new Country("USA", 316000000));
        countries.add(new Country("Mexico", 120000000));
        countries.add(new Country("Canada", 35000000));
    }

    public Collection<Country> getCountries() {
        return countries;
    }

    public Country getCountry(int countryId) {
        return countries.get(countryId);
    }
}
