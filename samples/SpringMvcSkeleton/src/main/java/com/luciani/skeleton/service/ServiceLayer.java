package com.luciani.skeleton.service;

import com.luciani.skeleton.model.Country;
import com.luciani.skeleton.persistence.PersistenceLayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Collection;

@Service
public class ServiceLayer {
    private static final Logger logger = LoggerFactory.getLogger(ServiceLayer.class);

    @Inject
    PersistenceLayer persistenceLayer;

    public Collection<Country> getCountries() {
        return persistenceLayer.getCountries();
    }

    public Country getCountry(int countryId) {
        return persistenceLayer.getCountry(countryId);
    }
}
