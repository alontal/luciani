package com.luciani.skeleton.controller;

import com.luciani.skeleton.model.Country;
import com.luciani.skeleton.service.ServiceLayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.inject.Inject;
import java.util.Collection;

@RequestMapping("/api/v1/countries")
@RestController
public class CountryController {
    private static final Logger logger = LoggerFactory.getLogger(CountryController.class);

    @Inject
    ServiceLayer serviceLayer;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Collection<Country> getCountries() {
        logger.info("Get all countries");
        // return "['USA', 'Canada', 'Mexico']";
        return serviceLayer.getCountries();
    }

    @RequestMapping(value = "/{countryId}", method = RequestMethod.GET)
    public Country getCountry(@PathVariable("countryId") int countryId) {
        logger.info("Get country with ID {}", countryId);
        return serviceLayer.getCountry(countryId);
    }

}
