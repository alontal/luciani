'use strict';
angular.module('SpringMvcSkeleton',
    ['SpringMvcSkeleton.countries'])
    .run(['$rootScope', '$state', function($rootScope, $state) {

        // Without this handler, some angular-ui-router
        // errors go undetected.
        // (Documented in https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions#issue-javascript-errors-dont-throw-within-resolve-functions)
        $rootScope.$on('$stateChangeError',
            function(event, toState, toParams, fromState, fromParams, error) {
                console.warn('$stateChangeError event fired. event=', event,
                    ', toState=', toState,
                    ', toParams=', toParams,
                    ', fromState=', fromState,
                    ', fromParams=', fromParams,
                    ', error=', error);
                throw error;
            });
        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams, error) {
               console.debug('Transitioned from state {', fromState.name, '} to state {', toState.name, '}');
            });
    }]);
