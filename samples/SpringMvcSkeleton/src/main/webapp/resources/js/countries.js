'use strict';
angular.module('SpringMvcSkeleton.countries',
    ['ui.router',
     'ngResource'])
    .controller('CountryController', ['$resource', function($resource) {
        var self = this;
        var Country = $resource('/SpringMvcSkeleton/api/v1/countries');
        self.countries = Country.query();
    }])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider.state('Countries', {
            templateUrl: 'resources/partials/countries.html',
            url: '/countries'
        });
    }]);
