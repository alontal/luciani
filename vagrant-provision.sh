yum update 
yum install -y httpd tomcat 

cat << EOF >> /etc/httpd/conf/httpd.conf 
LoadModule jk_module modules/mod_jk.so
JkMount       /luciani* luciani_worker
JkMount       /SpringMvcSkeleton* spring_mvc_skeleton_worker
JkWorkersFile /etc/httpd/conf/workers.properties
JkShmFile     /tmp/mod_jk.shm
JkLogLevel    warn
JkLogStampFormat "[%a %b %d %H:%M:%S %Y] "
EOF

cat << EOF >> /etc/httpd/conf/workers.properties
worker.list=luciani_worker, spring_mvc_skeleton_worker
worker.luciani_worker.type=ajp13
worker.luciani_worker.host=127.0.0.1
worker.luciani_worker.port=8009
worker.spring_mvc_skeleton_worker.type=ajp13
worker.spring_mvc_skeleton_worker.host=127.0.0.1
worker.spring_mvc_skeleton_worker.port=8009
EOF

cp /vagrant/mod_jk.so /etc/httpd/modules
cp /vagrant/samples/HelloWorldServlet/target/luciani.war /var/lib/tomcat/webapps/

chkconfig httpd on
chkconfig tomcat on

service tomcat restart
service httpd restart
